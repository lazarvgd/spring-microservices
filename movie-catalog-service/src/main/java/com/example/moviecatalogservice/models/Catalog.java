package com.example.moviecatalogservice.models;

import java.util.List;

public class Catalog {
    private List<CatalogItem> itemList;
    private int total = 976;
    private int page = 2;

    public Catalog(List<CatalogItem> itemList) {
        this.itemList = itemList;
    }

    public Catalog() {
    }

    public List<CatalogItem> getItemList() {
        return itemList;
    }

    public void setItemList(List<CatalogItem> itemList) {
        this.itemList = itemList;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }
}
